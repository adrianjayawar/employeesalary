package com.example.EmployeeSalary.modelsDTO;

public class KemampuanDTO {
	private int idKemampuan;
	private KategoriKemampuanDTO kategoriKemampuan;
	private String namaKemampuan;
	
	public KemampuanDTO() {

	}

	public KemampuanDTO(int idKemampuan, KategoriKemampuanDTO kategoriKemampuan, String namaKemampuan) {
		this.idKemampuan = idKemampuan;
		this.kategoriKemampuan = kategoriKemampuan;
		this.namaKemampuan = namaKemampuan;
	}

	public int getIdKemampuan() {
		return idKemampuan;
	}

	public void setIdKemampuan(int idKemampuan) {
		this.idKemampuan = idKemampuan;
	}

	public KategoriKemampuanDTO getKategoriKemampuan() {
		return kategoriKemampuan;
	}

	public void setKategoriKemampuan(KategoriKemampuanDTO kategoriKemampuan) {
		this.kategoriKemampuan = kategoriKemampuan;
	}

	public String getNamaKemampuan() {
		return namaKemampuan;
	}

	public void setNamaKemampuan(String namaKemampuan) {
		this.namaKemampuan = namaKemampuan;
	}
	
}

package com.example.EmployeeSalary.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeSalary.models.Pendapatan;

@Repository
public interface PendapatanRepository extends JpaRepository<Pendapatan, Integer> {

}

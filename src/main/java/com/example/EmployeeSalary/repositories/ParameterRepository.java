package com.example.EmployeeSalary.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeSalary.models.Parameter;

@Repository
public interface ParameterRepository extends JpaRepository<Parameter, Integer> {

}

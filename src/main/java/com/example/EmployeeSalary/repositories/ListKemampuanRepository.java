package com.example.EmployeeSalary.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeSalary.models.ListKemampuan;

@Repository
public interface ListKemampuanRepository extends JpaRepository<ListKemampuan, Integer> {

}

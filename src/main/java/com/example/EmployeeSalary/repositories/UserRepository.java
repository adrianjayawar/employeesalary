package com.example.EmployeeSalary.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.EmployeeSalary.models.User;
import com.example.EmployeeSalary.models.UserId;

@Repository
public interface UserRepository extends JpaRepository<User, UserId> {

}

package com.example.EmployeeSalary.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.PresentaseGaji;
import com.example.EmployeeSalary.modelsDTO.PresentaseGajiDTO;
import com.example.EmployeeSalary.repositories.PresentaseGajiRepository;

@RestController
@RequestMapping("/api")
public class PresentaseGajiController {
	@Autowired
	PresentaseGajiRepository presentasiGajiRepository;
	
	@GetMapping("/presentase_gaji/read")
	public HashMap<String, Object> getAllPresentaseGajiDTO() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<PresentaseGaji> listPresentaseGajiEntity = (ArrayList<PresentaseGaji>) presentasiGajiRepository.findAll();
		ArrayList<PresentaseGajiDTO> listPresentaseGajiDTO = new ArrayList<PresentaseGajiDTO>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		for(PresentaseGaji presentaseGaji : listPresentaseGajiEntity) {
			PresentaseGajiDTO presentaseGajiDTO = modelMapper.map(presentaseGaji, PresentaseGajiDTO.class);
			
			listPresentaseGajiDTO.add(presentaseGajiDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Presentase Gaji Data Success");
		result.put("Data", listPresentaseGajiDTO);
		
		return result;
	}
	
	@PostMapping("/presentase_gaji/create")
	public HashMap<String, Object> createPresentaseGajiDTO(@Valid @RequestBody PresentaseGajiDTO presentaseGajiDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		PresentaseGaji presentaseGajiEntity = modelMapper.map(presentaseGajiDTO, PresentaseGaji.class);
		
		presentasiGajiRepository.save(presentaseGajiEntity);
		
		presentaseGajiDTO.setIdPresentaseGaji(presentaseGajiEntity.getIdPresentaseGaji());
		
		result.put("Status", 200);
		result.put("Message", "Create New Presentase Gaji Success");
		result.put("Data", presentaseGajiDTO);
		
		return result;
	}
		
	@PutMapping("/presentase_gaji/update/{id}")
	public HashMap<String, Object> updatePresentaseGajiDTO(@PathVariable(value = "id") Integer idPresentaseGaji, @Valid @RequestBody PresentaseGajiDTO presentaseGajiDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		PresentaseGaji presentaseGajiEntity = presentasiGajiRepository.findById(idPresentaseGaji).orElseThrow(() -> new ResourceNotFoundException("PresentaseGaji", "id", idPresentaseGaji));
		presentaseGajiEntity = modelMapper.map(presentaseGajiDetails, PresentaseGaji.class);
		
		presentaseGajiEntity.setIdPresentaseGaji(idPresentaseGaji);

		presentasiGajiRepository.save(presentaseGajiEntity);
		
		presentaseGajiDetails = modelMapper.map(presentaseGajiEntity, PresentaseGajiDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Presentase Gaji Success");
		result.put("Data", presentaseGajiDetails);
		
		return result;
	}
	
	@DeleteMapping("/presentase_gaji/delete/{id}")
	public HashMap<String, Object> deletePresentaseGajiDTO(@PathVariable(value = "id") Integer idPresentaseGaji) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		PresentaseGaji presentaseGajiEntity = presentasiGajiRepository.findById(idPresentaseGaji).orElseThrow(() -> new ResourceNotFoundException("PresentaseGaji", "id", idPresentaseGaji));
		PresentaseGajiDTO presentaseGajiDTO = modelMapper.map(presentaseGajiEntity, PresentaseGajiDTO.class);
		
		presentasiGajiRepository.delete(presentaseGajiEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete Presentase Gaji Success");
		result.put("Data", presentaseGajiDTO);
		
		return result;
	}
}

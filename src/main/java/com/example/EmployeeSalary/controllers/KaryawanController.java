package com.example.EmployeeSalary.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.Karyawan;
import com.example.EmployeeSalary.modelsDTO.KaryawanDTO;
import com.example.EmployeeSalary.repositories.KaryawanRepository;

@RestController
@RequestMapping("/api")
public class KaryawanController {
	@Autowired
	KaryawanRepository karyawanRepository;
	
	@GetMapping("/karyawan/read")
	public HashMap<String, Object> getAllKaryawanDTO() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<Karyawan> listKaryawanEntity = (ArrayList<Karyawan>) karyawanRepository.findAll();
		ArrayList<KaryawanDTO> listKaryawanDTO = new ArrayList<KaryawanDTO>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		for(Karyawan karyawan : listKaryawanEntity) {
			KaryawanDTO karyawanDTO = modelMapper.map(karyawan, KaryawanDTO.class);
			
			listKaryawanDTO.add(karyawanDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Karyawan Data Success");
		result.put("Data", listKaryawanDTO);
		
		return result;
	}
	
	@PostMapping("/karyawan/create")
	public HashMap<String, Object> createKaryawanDTO(@Valid @RequestBody KaryawanDTO karyawanDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Karyawan karyawanEntity = modelMapper.map(karyawanDTO, Karyawan.class);
		
		karyawanRepository.save(karyawanEntity);
		
		karyawanDTO.setIdKaryawan(karyawanEntity.getIdKaryawan());
		
		result.put("Status", 200);
		result.put("Message", "Create New Karyawan Success");
		result.put("Data", karyawanDTO);
		
		return result;
	}
		
	@PutMapping("/karyawan/update/{id}")
	public HashMap<String, Object> updateKaryawanDTO(@PathVariable(value = "id") Integer idKaryawan, @Valid @RequestBody KaryawanDTO karyawanDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Karyawan karyawanEntity = karyawanRepository.findById(idKaryawan).orElseThrow(() -> new ResourceNotFoundException("Karyawan", "id", idKaryawan));
		karyawanEntity = modelMapper.map(karyawanDetails, Karyawan.class);
		
		karyawanEntity.setIdKaryawan(idKaryawan);;

		karyawanRepository.save(karyawanEntity);
		
		karyawanDetails = modelMapper.map(karyawanEntity, KaryawanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Karyawan Success");
		result.put("Data", karyawanDetails);
		
		return result;
	}
	
	@DeleteMapping("/karyawan/delete/{id}")
	public HashMap<String, Object> deleteKaryawanDTO(@PathVariable(value = "id") Integer idKaryawan) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Karyawan karyawanEntity = karyawanRepository.findById(idKaryawan).orElseThrow(() -> new ResourceNotFoundException("Karyawan", "id", idKaryawan));
		KaryawanDTO karyawanDTO = modelMapper.map(karyawanEntity, KaryawanDTO.class);
		
		karyawanRepository.delete(karyawanEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete Karyawan Success");
		result.put("Data", karyawanDTO);
		
		return result;
	}
}

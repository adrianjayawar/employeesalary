package com.example.EmployeeSalary.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.Tingkatan;
import com.example.EmployeeSalary.modelsDTO.TingkatanDTO;
import com.example.EmployeeSalary.repositories.TingkatanRepository;

@RestController
@RequestMapping("/api")
public class TingkatanController {
	@Autowired
	TingkatanRepository tingkatanRepository;
	
	@GetMapping("/tingkatan/read")
	public HashMap<String, Object> getAllTingkatanDTO() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<Tingkatan> listTingkatanEntity = (ArrayList<Tingkatan>) tingkatanRepository.findAll();
		ArrayList<TingkatanDTO> listTingkatanDTO = new ArrayList<TingkatanDTO>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		for(Tingkatan tingkatan : listTingkatanEntity) {
			TingkatanDTO tingkatanDTO = modelMapper.map(tingkatan, TingkatanDTO.class);
			
			listTingkatanDTO.add(tingkatanDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Tingkatan Data Success");
		result.put("Data", listTingkatanDTO);
		
		return result;
	}
	
	@PostMapping("/tingkatan/create")
	public HashMap<String, Object> createTingkatanDTO(@Valid @RequestBody TingkatanDTO tingkatanDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Tingkatan tingkatanEntity = modelMapper.map(tingkatanDTO, Tingkatan.class);
		
		tingkatanRepository.save(tingkatanEntity);
		
		tingkatanDTO.setIdTingkatan(tingkatanEntity.getIdTingkatan());
		
		result.put("Status", 200);
		result.put("Message", "Create New Tingkatan Success");
		result.put("Data", tingkatanDTO);
		
		return result;
	}
		
	@PutMapping("/tingkatan/update/{id}")
	public HashMap<String, Object> updateTingkatanDTO(@PathVariable(value = "id") Integer idTingkatan, @Valid @RequestBody TingkatanDTO tingkatanDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Tingkatan tingkatanEntity = tingkatanRepository.findById(idTingkatan).orElseThrow(() -> new ResourceNotFoundException("Tingkatan", "id", idTingkatan));
		tingkatanEntity = modelMapper.map(tingkatanDetails, Tingkatan.class);
		
		tingkatanEntity.setIdTingkatan(idTingkatan);

		tingkatanRepository.save(tingkatanEntity);
		
		tingkatanDetails = modelMapper.map(tingkatanEntity, TingkatanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Tingkatan Success");
		result.put("Data", tingkatanDetails);
		
		return result;
	}
	
	@DeleteMapping("/tingkatan/delete/{id}")
	public HashMap<String, Object> deleteTingkatanDTO(@PathVariable(value = "id") Integer idTingkatan) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Tingkatan tingkatanEntity = tingkatanRepository.findById(idTingkatan).orElseThrow(() -> new ResourceNotFoundException("Tingkatan", "id", idTingkatan));
		TingkatanDTO tingkatanDTO = modelMapper.map(tingkatanEntity, TingkatanDTO.class);
		
		tingkatanRepository.delete(tingkatanEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete Tingkatan Success");
		result.put("Data", tingkatanDTO);
		
		return result;
	}
}

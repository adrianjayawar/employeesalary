package com.example.EmployeeSalary.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.Posisi;
import com.example.EmployeeSalary.modelsDTO.PosisiDTO;
import com.example.EmployeeSalary.repositories.PosisiRepository;

@RestController
@RequestMapping("/api")
public class PosisiController {
	@Autowired
	PosisiRepository posisiRepository;
	
	@GetMapping("/posisi/read")
	public HashMap<String, Object> getAllPosisiDTO() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<Posisi> listPosisiEntity = (ArrayList<Posisi>) posisiRepository.findAll();
		ArrayList<PosisiDTO> listPosisiDTO = new ArrayList<PosisiDTO>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		for(Posisi posisi : listPosisiEntity) {
			PosisiDTO posisiDTO = modelMapper.map(posisi, PosisiDTO.class);
			
			listPosisiDTO.add(posisiDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Posisi Data Success");
		result.put("Data", listPosisiDTO);
		
		return result;
	}
	
	@PostMapping("/posisi/create")
	public HashMap<String, Object> createPosisiDTO(@Valid @RequestBody PosisiDTO posisiDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Posisi posisiEntity = modelMapper.map(posisiDTO, Posisi.class);
		
		posisiRepository.save(posisiEntity);
		
		posisiDTO.setIdPosisi(posisiEntity.getIdPosisi());
		
		result.put("Status", 200);
		result.put("Message", "Create New Posisi Success");
		result.put("Data", posisiDTO);
		
		return result;
	}
		
	@PutMapping("/posisi/update/{id}")
	public HashMap<String, Object> updatePosisiDTO(@PathVariable(value = "id") Integer idPosisi, @Valid @RequestBody PosisiDTO posisiDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Posisi posisiEntity = posisiRepository.findById(idPosisi).orElseThrow(() -> new ResourceNotFoundException("Posisi", "id", idPosisi));
		posisiEntity = modelMapper.map(posisiDetails, Posisi.class);
		
		posisiEntity.setIdPosisi(idPosisi);

		posisiRepository.save(posisiEntity);
		
		posisiDetails = modelMapper.map(posisiEntity, PosisiDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Posisi Success");
		result.put("Data", posisiDetails);
		
		return result;
	}
	
	@DeleteMapping("/posisi/delete/{id}")
	public HashMap<String, Object> deletePosisiDTO(@PathVariable(value = "id") Integer idPosisi) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Posisi posisiEntity = posisiRepository.findById(idPosisi).orElseThrow(() -> new ResourceNotFoundException("Posisi", "id", idPosisi));
		PosisiDTO posisiDTO = modelMapper.map(posisiEntity, PosisiDTO.class);
		
		posisiRepository.delete(posisiEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete Posisi Success");
		result.put("Data", posisiDTO);
		
		return result;
	}
}

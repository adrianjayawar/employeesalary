package com.example.EmployeeSalary.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.Agama;
import com.example.EmployeeSalary.modelsDTO.AgamaDTO;
import com.example.EmployeeSalary.repositories.AgamaRepository;

@RestController
@RequestMapping("/api")
public class AgamaController {
	@Autowired
	AgamaRepository agamaRepository;
	
	@GetMapping("/agama/read")
	public HashMap<String, Object> getAllAgamaDTO() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<Agama> listAgamaEntity = (ArrayList<Agama>) agamaRepository.findAll();
		ArrayList<AgamaDTO> listAgamaDTO = new ArrayList<AgamaDTO>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		for(Agama agama : listAgamaEntity) {
			AgamaDTO agamaDTO = modelMapper.map(agama, AgamaDTO.class);
			
			listAgamaDTO.add(agamaDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Agama Data Success");
		result.put("Data", listAgamaDTO);
		
		return result;
	}
	
	@PostMapping("/agama/create")
	public HashMap<String, Object> createAgamaDTO(@Valid @RequestBody AgamaDTO agamaDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Agama agamaEntity = modelMapper.map(agamaDTO, Agama.class);
		
		agamaRepository.save(agamaEntity);
		
		agamaDTO.setIdAgama(agamaEntity.getIdAgama());
		
		result.put("Status", 200);
		result.put("Message", "Create New Agama Success");
		result.put("Data", agamaDTO);
		
		return result;
	}
		
	@PutMapping("/agama/update/{id}")
	public HashMap<String, Object> updateAgamaDTO(@PathVariable(value = "id") Integer idAgama, @Valid @RequestBody AgamaDTO agamaDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Agama agamaEntity = agamaRepository.findById(idAgama).orElseThrow(() -> new ResourceNotFoundException("Agama", "id", idAgama));
		agamaEntity = modelMapper.map(agamaDetails, Agama.class);
		
		agamaEntity.setIdAgama(idAgama);

		agamaRepository.save(agamaEntity);
		
		agamaDetails = modelMapper.map(agamaEntity, AgamaDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Agama Success");
		result.put("Data", agamaDetails);
		
		return result;
	}
	
	@DeleteMapping("/agama/delete/{id}")
	public HashMap<String, Object> deleteAgamaDTO(@PathVariable(value = "id") Integer idAgama) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Agama agamaEntity = agamaRepository.findById(idAgama).orElseThrow(() -> new ResourceNotFoundException("Agama", "id", idAgama));
		AgamaDTO agamaDTO = modelMapper.map(agamaEntity, AgamaDTO.class);
		
		agamaRepository.delete(agamaEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete Agama Success");
		result.put("Data", agamaDTO);
		
		return result;
	}
}

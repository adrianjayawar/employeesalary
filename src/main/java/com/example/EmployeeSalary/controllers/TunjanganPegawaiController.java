package com.example.EmployeeSalary.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.TunjanganPegawai;
import com.example.EmployeeSalary.modelsDTO.TunjanganPegawaiDTO;
import com.example.EmployeeSalary.repositories.TunjanganPegawaiRepository;

@RestController
@RequestMapping("/api")
public class TunjanganPegawaiController {
	@Autowired
	TunjanganPegawaiRepository tunjanganPegawaiRepository;
	
	@GetMapping("/tunjangan_pegawai/read")
	public HashMap<String, Object> getAllTunjanganPegawaiDTO() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<TunjanganPegawai> listTunjanganPegawaiEntity = (ArrayList<TunjanganPegawai>) tunjanganPegawaiRepository.findAll();
		ArrayList<TunjanganPegawaiDTO> listTunjanganPegawaiDTO = new ArrayList<TunjanganPegawaiDTO>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		for(TunjanganPegawai tunjanganPegawai : listTunjanganPegawaiEntity) {
			TunjanganPegawaiDTO tunjanganPegawaiDTO = modelMapper.map(tunjanganPegawai, TunjanganPegawaiDTO.class);
			
			listTunjanganPegawaiDTO.add(tunjanganPegawaiDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Tunjangan Pegawai Data Success");
		result.put("Data", listTunjanganPegawaiDTO);
		
		return result;
	}
	
	@PostMapping("/tunjangan_pegawai/create")
	public HashMap<String, Object> createTunjanganPegawaiDTO(@Valid @RequestBody TunjanganPegawaiDTO tunjanganPegawaiDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		TunjanganPegawai tunjanganPegawaiEntity = modelMapper.map(tunjanganPegawaiDTO, TunjanganPegawai.class);
		
		tunjanganPegawaiRepository.save(tunjanganPegawaiEntity);
		
		tunjanganPegawaiDTO.setIdTunjanganPegawai(tunjanganPegawaiEntity.getIdTunjanganPegawai());
		
		result.put("Status", 200);
		result.put("Message", "Create New Tunjangan Pegawai Success");
		result.put("Data", tunjanganPegawaiDTO);
		
		return result;
	}
		
	@PutMapping("/tunjangan_pegawai/update/{id}")
	public HashMap<String, Object> updateTunjanganPegawaiDTO(@PathVariable(value = "id") Integer idTunjanganPegawai, @Valid @RequestBody TunjanganPegawaiDTO tunjanganPegawaiDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		TunjanganPegawai tunjanganPegawaiEntity = tunjanganPegawaiRepository.findById(idTunjanganPegawai).orElseThrow(() -> new ResourceNotFoundException("TunjanganPegawai", "id", idTunjanganPegawai));
		tunjanganPegawaiEntity = modelMapper.map(tunjanganPegawaiDetails, TunjanganPegawai.class);
		
		tunjanganPegawaiEntity.setIdTunjanganPegawai(idTunjanganPegawai);

		tunjanganPegawaiRepository.save(tunjanganPegawaiEntity);
		
		tunjanganPegawaiDetails = modelMapper.map(tunjanganPegawaiEntity, TunjanganPegawaiDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Tunjangan Pegawai Success");
		result.put("Data", tunjanganPegawaiDetails);
		
		return result;
	}
	
	@DeleteMapping("/tunjangan_pegawai/delete/{id}")
	public HashMap<String, Object> deleteTunjanganPegawaiDTO(@PathVariable(value = "id") Integer idTunjanganPegawai) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		TunjanganPegawai tunjanganPegawaiEntity = tunjanganPegawaiRepository.findById(idTunjanganPegawai).orElseThrow(() -> new ResourceNotFoundException("TunjanganPegawai", "id", idTunjanganPegawai));
		TunjanganPegawaiDTO tunjanganPegawaiDTO = modelMapper.map(tunjanganPegawaiEntity, TunjanganPegawaiDTO.class);
		
		tunjanganPegawaiRepository.delete(tunjanganPegawaiEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete Tunjangan Pegawai Success");
		result.put("Data", tunjanganPegawaiDTO);
		
		return result;
	}
}

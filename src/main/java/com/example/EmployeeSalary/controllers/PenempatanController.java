package com.example.EmployeeSalary.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.EmployeeSalary.exceptions.ResourceNotFoundException;
import com.example.EmployeeSalary.models.Penempatan;
import com.example.EmployeeSalary.modelsDTO.PenempatanDTO;
import com.example.EmployeeSalary.repositories.PenempatanRepository;

@RestController
@RequestMapping("/api")
public class PenempatanController {
	@Autowired
	PenempatanRepository penempatanRepository;
	
	@GetMapping("/penempatan/read")
	public HashMap<String, Object> getAllPenempatanDTO() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<Penempatan> listPenempatanEntity = (ArrayList<Penempatan>) penempatanRepository.findAll();
		ArrayList<PenempatanDTO> listPenempatanDTO = new ArrayList<PenempatanDTO>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		for(Penempatan penempatan : listPenempatanEntity) {
			PenempatanDTO penempatanDTO = modelMapper.map(penempatan, PenempatanDTO.class);
			
			listPenempatanDTO.add(penempatanDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Penempatan Data Success");
		result.put("Data", listPenempatanDTO);
		
		return result;
	}
	
	@PostMapping("/penempatan/create")
	public HashMap<String, Object> createPenempatanDTO(@Valid @RequestBody PenempatanDTO penempatanDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Penempatan penempatanEntity = modelMapper.map(penempatanDTO, Penempatan.class);
		
		penempatanRepository.save(penempatanEntity);
		
		penempatanDTO.setIdPenempatan(penempatanEntity.getIdPenempatan());
		
		result.put("Status", 200);
		result.put("Message", "Create New Penempatan Success");
		result.put("Data", penempatanDTO);
		
		return result;
	}
		
	@PutMapping("/penempatan/update/{id}")
	public HashMap<String, Object> updatePenempatanDTO(@PathVariable(value = "id") Integer idPenempatan, @Valid @RequestBody PenempatanDTO penempatanDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Penempatan penempatanEntity = penempatanRepository.findById(idPenempatan).orElseThrow(() -> new ResourceNotFoundException("Penempatan", "id", idPenempatan));
		penempatanEntity = modelMapper.map(penempatanDetails, Penempatan.class);
		
		penempatanEntity.setIdPenempatan(idPenempatan);

		penempatanRepository.save(penempatanEntity);
		
		penempatanDetails = modelMapper.map(penempatanEntity, PenempatanDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Penempatan Success");
		result.put("Data", penempatanDetails);
		
		return result;
	}
	
	@DeleteMapping("/penempatan/delete/{id}")
	public HashMap<String, Object> deletePenempatanDTO(@PathVariable(value = "id") Integer idPenempatan) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Penempatan penempatanEntity = penempatanRepository.findById(idPenempatan).orElseThrow(() -> new ResourceNotFoundException("Penempatan", "id", idPenempatan));
		PenempatanDTO penempatanDTO = modelMapper.map(penempatanEntity, PenempatanDTO.class);
		
		penempatanRepository.delete(penempatanEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete Penempatan Success");
		result.put("Data", penempatanDTO);
		
		return result;
	}
}
